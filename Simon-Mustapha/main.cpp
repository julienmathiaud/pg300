#include <iostream>
#include <vector>
#include <random>
#include <fstream>
#include <time.h>
#include <cmath>


using namespace std;

double logar(double s) //Approximation of log(s)/s
{
  return -(1+(1-s)*0.5+(1-s)*(1-s)*0.33)*((1/s)-1);
}

inline float calcinvsqroot( float n ) { //Approximation of 1/sqrt(x)

   const float threehalfs = 1.5f;
   float y = n;

   long i = * ( long * ) &y;

   i = 0x5f3759df - ( i >> 1 );
   y = * ( float * ) &i;

   y = y * ( threehalfs - ( (n * 0.5f) * y * y ) );

   return y;
}

double generateGaussian() //Algorithm of Box-Muller, it generates a gaussien distribution
{
    double u, v, s;
    do {
        u = (rand() / ((double)RAND_MAX)) * 2.0 - 1.0;
        v = (rand() / ((double)RAND_MAX)) * 2.0 - 1.0;

        s = u * u + v * v;
    } while (s >= 1.0 || s == 0.0);
    s =sqrt(-2.0 * log(s) / s);
    //s=calcinvsqroot(-0.5*s/log(s));

    return u * s;

}

int main()
{
  clock_t time;
  time=clock();
  ofstream my_T,my_E,my_mean;
  double tau(5),dt(0.01);
  int R(280),Np(500000);
  vector <double> Energ(Np),T(floor(tau/dt)+2),E(floor(tau/dt)+2);
  double MoyEnerg(0);


  //default_random_engine generator;
  //normal_distribution<double> distrib(0.0,1.0);
  double sigma;

  //INITIALISATION
  for (int i=0;i<Np;++i)
  {
    Energ[i] = rand() % 100000 + 100000;
    MoyEnerg += Energ[i];
  }
  //double Tconst(MoyEnerg/(Np*R));
  E[0]=MoyEnerg/Np;
  T[0]=MoyEnerg/(Np*R);
  double t(0);
  int iteration(0);

  //BOUCLE
  double d_tau=R*dt/tau;
  double alpha=1/(1+2*dt/tau);

  while (t<tau)
  {
    MoyEnerg=0.0;



    for (int i=0;i<Np;++i)
    {
      sigma = generateGaussian();

      Energ[i]=alpha*(Energ[i] + d_tau*T[iteration]*(1+sigma*sigma)+2*sqrt(d_tau*T[iteration]*Energ[i])*sigma);

      MoyEnerg += Energ[i];
    }
    iteration+=1;
    E[iteration]=MoyEnerg/(Np);
    T[iteration]=MoyEnerg/(Np*R);

    t+=dt;
  }
  time=(clock()-time);///CLOCKS_PER_SEC;
  //PRINT
  my_T.open("T.txt");
  my_E.open("E.txt");
  my_mean.open("mean.txt");
  for (int i=0;i<iteration;++i)
  {
    my_T << i << " "<< T[i] << endl;
  }
  for (int i=0;i<Np;++i)
  {
    my_E << Energ[i] << endl;
  }
  for (int i=0;i<iteration;++i)
  {
    my_mean << i << " " << E[i] << endl;
  }
  my_T.close();
  my_E.close();

  cout<<"It took me "<<time<<endl;
  return 0;
}
