program energie

        implicit none

        real :: tau, dt, R, T, sigma, sigma2, pi
        integer :: Np, i, n, ntime

        real :: const1, const2,const3, const4, const5

        real, dimension(:), allocatable :: Ep

        tau = 5d0
        dt = 0.01
        Np = 500000
        R = 280d0
        pi = 4d0*atan(1d0)
        ntime = 500
        T = 0d0

        const2 = 1d0/(1d0+2d0*dt/tau)
        const3 = R*dt/tau
        const4 = 1d0/(1d0*Np*R)

        open(unit=10,file='moyenne.txt')
        open(unit=11,file='energie.txt')

        allocate(Ep(Np))

        Ep = 0d0

        do i = 1, Np
                Ep(i) = rand()*100000d0 + 100000d0
                T = T + Ep(i)        
        end do

        T = T*const4
        write(10,*) 1, T

        do n = 1, ntime
                const1 = const3*T       
                const5 = 2d0*sqrt(const1)
                T = 0d0
                do i = 1, Np,2
                        sigma  = sqrt(-2d0*log(1-rand()))*cos(2d0*pi*rand())
                        sigma2 = sqrt(-2d0*log(1-rand()))*sin(2d0*pi*rand())
                        Ep(i) = const2*(Ep(i)+const1*(1d0+sigma**2)+ sigma*const5*sqrt(Ep(i))) 
                        Ep(i+1) = const2*(Ep(i+1)+const1*(1d0+sigma2**2)+ sigma2*const5*sqrt(Ep(i+1))) 
                        T = T + Ep(i) + Ep(i+1)
                end do
                T = T*const4
                write(10,*) n+1, T
        end do          


        do i = 1, Np
                write(11,*) Ep(i)
        end do
        close(10)
        close(11)

 
        deallocate(Ep)

end program energie

