program energie

        implicit none

        real :: tau, dt, R, T, sigma, pi
        integer :: Np, i, n, ntime


        real, dimension(:), allocatable :: Ep,tab_T
         
        tau = 5d0
        dt = 0.01
        Np = 500000
        R = 280d0
        pi = 4d0*atan(1d0)
        ntime = 500
        T = 0d0

        allocate(Ep(Np))
        allocate(tab_T(ntime+1))

        tab_T = 0d0
        Ep = 0d0

        do i = 1, Np
                Ep(i) = rand()*100000d0 + 100000d0
        end do

        T = sum(Ep)/(1d0*Np*R)
        tab_T(1) = T

        do n = 1, ntime
                do i = 1, Np
                        sigma = sqrt(-2d0*log(1-rand()))*cos(2d0*pi*rand())
                        Ep(i) = (1d0/(1d0+2d0*dt/tau))*(Ep(i)+(R*T*dt/tau)*(1d0+sigma**2)+ 2d0*sigma*sqrt(dt*R*T*Ep(i)/tau)) 
                end do
                T = sum(Ep)/(1d0*Np*R)
                tab_T(n+1) = T
        end do   
        

        open(unit=10,file='moyenne.txt')
        do n = 1, ntime+1
                write(10,*) n, tab_T(n)
        end do
        close(10)

        open(unit=11,file='energie.txt')
        do i = 1, Np
                write(11,*) Ep(i)
        end do
        close(11)

 
        deallocate(Ep)
        deallocate(tab_T)

end program energie

